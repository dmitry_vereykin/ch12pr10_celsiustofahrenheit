/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.text.DecimalFormat;

public class CelsiusToFahrenheit extends JFrame {
    private JButton covertToCel;
    private JButton covertToFahr;
    private JTextField celsiusField;
    private JTextField fahrenheitField;

    public CelsiusToFahrenheit() {
        this.setTitle("Celsius/Fahrenheit Converter");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GridLayout layout = new GridLayout(2, 3);
        this.setLayout(layout);

        JLabel celsiusL = new JLabel(" Celsius ");
        celsiusField = new JTextField("0", 5);
        covertToFahr = new JButton(" <To Fahrenheit> ");
        JLabel fahrenheitL = new JLabel(" Fahrenheit ");
        fahrenheitField = new JTextField("0", 5);
        covertToCel = new JButton(" <To Celsius> ");

        covertToCel.addActionListener(new ToCelsiusCelListener());
        covertToFahr.addActionListener(new ToFahrenheitListener());

        this.add(celsiusL);
        this.add(celsiusField);
        this.add(covertToFahr);
        this.add(fahrenheitL);
        this.add(fahrenheitField);
        this.add(covertToCel);

        setSize(420, 90);
        setVisible(true);
        setLocationRelativeTo(null);
    }

    private class ToCelsiusCelListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            DecimalFormat niceD = new DecimalFormat("0.0");

            try {
                double fahrenheit = Double.parseDouble(fahrenheitField.getText());
                float answerCel = (float) ((fahrenheit - 32) * 5) / 9;
                celsiusField.setText(niceD.format(answerCel));
            } catch (Exception exc) {
                JOptionPane.showMessageDialog(null, "ERROR. Entered value is not a number.");
            }
        }
    }

    private class ToFahrenheitListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            DecimalFormat niceD = new DecimalFormat("0.00");

            try {
                double celsius = Double.parseDouble(celsiusField.getText());
                float answerFahr = (float) ((9 * celsius) / 5) + 32;
                fahrenheitField.setText(niceD.format(answerFahr));
            } catch (Exception exc) {
                JOptionPane.showMessageDialog(null, "ERROR. Entered value is not a number.");
            }
        }
    }

    public static void main(String[] args) {
        new CelsiusToFahrenheit();
    }

}
